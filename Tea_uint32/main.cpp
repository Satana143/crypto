#include <iostream>
#include <fstream>
#include <string>
#include <stdint.h>
#include <stdlib.h>
#include <vector>

using namespace std;

string newfile;

string getNewFileName(string file){
    //генерим новое имя файла

    int poz=file.find(".",0);
    string prefix = file.substr(poz,file.length()-poz);
    string newfile=file.substr(0,poz);
    newfile+="_copy"+prefix;
    return newfile;
}

void code (uint32_t *_v, uint32_t *_v1, uint32_t* k) {
    uint32_t v0=*_v, v1=*_v1,  i;
    uint32_t delta=0x9e3779b9;
    uint32_t k0=k[0], k1=k[1], k2=k[2], k3=k[3];
    for (i=0; i < 32; i++) {
        v0 += ((v1<<4) + k0) ^ (v1 + delta) ^ ((v1>>5) + k1);
        v1 += ((v0<<4) + k2) ^ (v0 + delta) ^ ((v0>>5) + k3);
    }
    *_v=v0; *_v1=v1;
}

void decode (uint32_t *_v, uint32_t *_v1, uint32_t* k) {
    uint32_t v0=*_v, v1=*_v1, i;
    uint32_t delta=0x9e3779b9;
    uint32_t k0=k[0], k1=k[1], k2=k[2], k3=k[3];
    for (i=0; i<32; i++) {
        v1 -= ((v0<<4) + k2) ^ (v0 + delta) ^ ((v0>>5) + k3);
        v0 -= ((v1<<4) + k0) ^ (v1 + delta) ^ ((v1>>5) + k1);
    }
    *_v=v0; *_v1=v1;
}

int main()
{
    srand(0);
    string file;
    bool answer;
    uint32_t *keyArr=new uint32_t[4];

    while(1){
            int encDeC;
            cout<<"Encrypt a file: 1"<<endl;
            cout<<"Decrypt the file: 2"<<endl;
            cin>>encDeC;

            if(encDeC==1){
            answer=true;
            break;
            }
            if(encDeC==2){
                answer=false;
                break;
            }
            cout<<"Unknown parameter"<<endl<<endl;
        }

        cout << "Path to file!"<<endl;
        cin >> file;
        newfile=getNewFileName(file);

        ifstream in(file.c_str(), ios::in | ios::binary); //входящий поток
        ofstream out(newfile.c_str(),ios::out|ios::binary); //исходящий поток

        //корректность открытия файла
        if(!in){
            cout<< "Error open file!"<<endl;
            return 1;
        }
        if(!out){
            cout<<"Out File not create!"<<endl;
            return 1;
        }


        if(answer){
            ofstream keyFile("key.txt",ios::out|ios::binary); //файл ключей
            if(!keyFile){
                cout<<"Key File not create!"<<endl;
                return 1;
            }
            //генерим 4 части ключа
               for(int i=0;i<4;i++){
                    keyArr[i]=rand()%256;
                    keyFile<<keyArr[i]<<"\n";
                }
                keyFile.close();
        }else{
            string keyInPath;
            cout<<"Path to key file"<<endl;
            cin>>keyInPath;
            ifstream keyInFile("key.txt",ios::in|ios::binary); //файл ключей
            if(!keyInFile){
                cout<<"Error open Key file!"<<endl;
                return 1;
            }

            //читаем ключи
            string buf;
            int i=0;
            while ( keyInFile >> buf ) {
                keyArr[i]=atoi(buf.c_str());
                i++;
               }
            keyInFile.close();
        }

/*---------------------------------*/
        uint32_t *tmp =new uint32_t  ;//блок рас
        uint32_t *tmp1 = new uint32_t ;//блок два
            *tmp ^= *tmp;

//читаем файл блоками по 4 байта

    while(in){
        in.read((char*)tmp,4);//читаем
        in.read((char*)tmp1,4);//еще раз

        if(answer)
            code(tmp,tmp1,keyArr);//шифруем
        else
            decode(tmp,tmp1,keyArr);//расшифровываем

        out.write(reinterpret_cast<const char*>(tmp),sizeof(tmp));//пишим
        out.write(reinterpret_cast<const char*>(tmp1),sizeof(tmp));//пищим
    }
    in.close();

    out.close();


    cout<<"Copy is complete! :)"<<endl;

    return 0;
}
