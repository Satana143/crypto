#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <vector>

using namespace std;

string getNewFileName(string file){
    //генерим новое имя файла

    int poz=file.find(".",0);
    string prefix = file.substr(poz,file.length()-poz);
    string newfile=file.substr(0,poz);
    newfile+="_crypto"+prefix;
    return newfile;
}

vector<string> readKeyFile(ifstream file){
    //читаем файл ключей
    string buf;
    vector<string> text;
    while ( file >> buf ) {
             text.push_back( buf );
       }
       return text;
}

long powmod(long a, long k)
{
  long b=1;

  while (k) {
    if (k%2==0) {
      k /= 2;
      a *= a;
      }
    else {
      k--;
      b *= a;
      }
  }
  return b;
}

void encrypt(){
    string file,newfile;

        cout << "Path to your encrypted file!"<<endl;
        cin >> file;
        newfile=getNewFileName(file);

    ifstream in(file.c_str(), ios::in | ios::binary); //входящий поток

    ofstream out(newfile.c_str(),ios::out|ios::binary); //исходящий поток
    ofstream outKey("key.txt");//файл ключей

    //корректность открытия файла
    if(!in){
        cout<< "Error open file!"<<endl;
        exit(1);
    }
    if(!out){
        cout<<"File not create!"<<endl;
        exit(2);
    }

    if(!outKey){
        cout<<"File not create!"<<endl;
        exit(2);
    }

    char buf; //буфер
    char key1 = rand()%256 | 1;
    char key2 = rand()%256;

    outKey<<key1;
    outKey<<"\n";
    outKey<<key2;

    //копируем файл
    while(in){
        in.get(buf);
        if(in) {
            char bi=(buf*key1 + key2);

                if(bi>256) bi-=256;

            out.put(bi);
        }
    }

    in.close();
    out.close();
    outKey.close();
    cout<<"Encrypted is complete! :)"<<endl;
}

void decrypt(){
    string file,newfile,keyfile;

        cout << "Path to decrypt files!"<<endl;
        cin >> file;
        newfile=getNewFileName(file);

        cout<< "Path to keys file!"<<endl;
        cin >> keyfile;

    ifstream in(file.c_str(), ios::in | ios::binary); //входящий поток
    ifstream key(keyfile.c_str());

    ofstream out(newfile.c_str(),ios::out|ios::binary); //исходящий поток

    //корректность открытия файла
    if(!in){
        cout<< "Error open file!"<<endl;
        exit(1);
    }
    if(!key){
        cout<<"Error open file!"<<endl;
        exit(2);
    }
    if(!out){
        cout<<"File not create!"<<endl;
        exit(2);
    }

    //расшифровываем
    char bi;

//считывание ключей из файла
    string buf;
    vector<string> text;
    while ( key >> buf ) {
             text.push_back( buf );
       }

string buf1 = text[0];
string buf2 = text[1];


char key1 = buf1[0];//получение ключа из строки
long key1_pow = powmod(key1,127); //возводим в степень
char key1_pow_convert =(char)key1_pow; //переводим в Char

char key2 = buf2[0]; //получаем ключ из строки

    while(in){
        in.get(bi);
        if(in){
            char ai=(bi-key2)*key1_pow_convert;
                if(ai<0) ai+=256;
            out.put(ai);
        }
    }

    in.close();
    key.close();
    out.close();
    cout << "Decrypt is complete!"<<endl;
}

int main()
{
    while(1){
        int encDeC;
        cout<<"Encrypt a file: 1"<<endl;
        cout<<"Decrypt the file: 2"<<endl;
        cin>>encDeC;

        if(encDeC==1){
        encrypt();
        break;
        }
        if(encDeC==2){
            decrypt();
            break;
        }
        cout<<"Unknown parameter"<<endl<<endl;
    }
    return 0;
}
