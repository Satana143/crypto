#include <iostream>
#include <io.h>
#include <fstream>
#include <string>
#include <string.h>
#include <vector>

using namespace std;

struct hash{
    int count;
    char symbol;
};

vector<hash> list;

string getNewFileName(string file){
    //генерим новое имя файла
    int poz=file.find(".",0);
    string prefix = file.substr(poz,file.length()-poz);
    string newfile=file.substr(0,poz);
    newfile+="_copy"+prefix;
    return newfile;
}

long powmod(long a, long k)
{
  long b=1;

  while (k) {
    if (k%2==0) {
      k /= 2;
      a *= a;
      }
    else {
      k--;
      b *= a;
      }
  }
  return b;
}
int main()
{

    string file,newfile;

        cout << "Path to file!"<<endl;
        cin >> file;
        newfile=getNewFileName(file);

    ifstream in(file.c_str(), ios::in | ios::binary); //входящий поток
    ofstream out(newfile.c_str(),ios::out|ios::binary); //исходящий поток

    //корректность открытия файла
    if(!in){
        cout<< "Error open file!"<<endl;
        return 1;
    }
    if(!out){
        cout<<"File not create!"<<endl;
        return 2;
    }

    char buf;
    unsigned int arr[256];
    for(int i=0;i<256;i++) arr[i]=0; //зануляем

    //читаем
    while(in){
        in.get(buf);
        if(in){
            arr[buf]++;
        }
    }
    in.close();

    //максимум
    int id=0;
    int id2=0;
    int max=0;
    int max2=0;

    for(int i=0;i<256;i++){
        if(arr[i]>id){
            max=i;
            id=arr[i];
        }
    }

    for(int i=0;i<256;i++){
        if(arr[i]<arr[max] && arr[i]>id2){
            max2=i;
            id2=arr[i];
        }
    }


    char spaceC=max;
    char eC=max2;
    /*----------------------------------------------*/
    //поиск ключей
    char e=101;
    char space=32;

    char k1=(spaceC-eC)*powmod(space-e,127)%256;
    char k2 =(spaceC-space*k1)%256;
    //расшифровываем
    ifstream in2(file.c_str(), ios::in | ios::binary); //входящий поток2
    char bi;
    while(in2){
        in2.get(bi);
        if(in2){
            char ai=((bi-k2)*powmod(k1,127))%256;
            out.put(ai);
        }
    }

    in2.close();
    out.close();

    cout<<"Complete decrypt";

    return 0;
}
