#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <vector>

using namespace std;

string getNewFileName(string file){
    //генерим новое имя файла

    int poz=file.find(".",0);
    string prefix = file.substr(poz,file.length()-poz);
    string newfile=file.substr(0,poz);
    newfile+="_crypto"+prefix;
    return newfile;
}
void encrypt(){
    string file,newfile;

        cout << "Path to your encrypted file!"<<endl;
        cin >> file;
        newfile=getNewFileName(file);

    ifstream in(file.c_str(), ios::in | ios::binary); //входящий поток

    ofstream out(newfile.c_str(),ios::out|ios::binary); //исходящий поток
    ofstream outKey("key.txt");//файл ключей

    //корректность открытия файла
    if(!in){
        cout<< "Error open file!"<<endl;
        exit(1);
    }
    if(!out){
        cout<<"File not create!"<<endl;
        exit(2);
    }

    if(!outKey){
        cout<<"File not create!"<<endl;
        exit(2);
    }

    char buf; //буфер

    //копируем файл
    while(in){
        in.get(buf);
        if(in) {
            //формируем ключ и записываем в файл
            int key = rand()%100;
            outKey<<key<<"\n";
            char bi=buf+key;

                if (bi>255) bi-=255;

            out.put(bi);
        }
    }

    in.close();
    out.close();
    outKey.close();
    cout<<"Encrypted is complete! :)"<<endl;
}

void decrypt(){
    string file,newfile,keyfile;

        cout << "Path to decrypt files!"<<endl;
        cin >> file;
        newfile=getNewFileName(file);

        cout<< "Path to keys file!"<<endl;
        cin >> keyfile;

    ifstream in(file.c_str(), ios::in | ios::binary); //входящий поток
    ifstream key(keyfile.c_str());

    ofstream out(newfile.c_str(),ios::out|ios::binary); //исходящий поток

    //корректность открытия файла
    if(!in){
        cout<< "Error open file!"<<endl;
        exit(1);
    }
    if(!key){
        cout<<"Error open file!"<<endl;
        exit(2);
    }
    if(!out){
        cout<<"File not create!"<<endl;
        exit(2);
    }

    //читаем файл ключей
    string buf;
    vector<string> text;
    while ( key >> buf ) {
             text.push_back( buf );
       }

    //расшифровываем
    char bi;
    int i=0;
    while(in){
        in.get(bi);
        if(in){
            int k=atoi(text[i].c_str());
            char ai=bi-k;
                if(ai<0) ai+=256;
            out.put(ai);
            i++;
        }
    }

    in.close();
    key.close();
    out.close();
    cout << "Decrypt is complete!"<<endl;
}

int main()
{
    while(1){
        int encDeC;
        cout<<"Encrypt a file: 1"<<endl;
        cout<<"Decrypt the file: 2"<<endl;
        cin>>encDeC;

        if(encDeC==1){
        encrypt();
        break;
        }
        if(encDeC==2){
            decrypt();
            break;
        }
        cout<<"Unknown parameter"<<endl<<endl;
    }
    return 0;
}
