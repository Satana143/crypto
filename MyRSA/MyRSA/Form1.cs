﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Numerics;
using System.Security.Cryptography;

namespace MyRSA
{
    public partial class Form1 : Form
    {
        RSA4fun rsa;
        public Form1()
        {
            InitializeComponent();       
             rsa= new RSA4fun();    
        }

       

        private void button1_Click_1(object sender, EventArgs e)
        {
            console.Clear();

            string text = textBox1.Text;
            byte[] message = Encoding.ASCII.GetBytes(text);
            rsa.initKey();
            
            console.AppendText("Поехали!\n");

            byte[] crypto = rsa.crypto(message);
            byte[] decrypt = rsa.decrypt(crypto);

            textBox2.Text = Encoding.ASCII.GetString(crypto);
            textBox3.Text = Encoding.ASCII.GetString(decrypt);

            
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            consoleCP.Clear();
            rsa.initKey();
            string m = textCP.Text;
            byte[] _m = Encoding.ASCII.GetBytes(m);
            byte[] md5 = MD5.Create().ComputeHash(_m);
            byte[] s= rsa.decrypt(md5);

             messageCP.Text = textCP.Text;
             string outS="";
             foreach (byte b in s)
             {
                 outS += (char)b;
             }
             signatureCP.Text = outS;
             consoleCP.AppendText(Encoding.ASCII.GetString(s) + "\n");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            consoleCP2.Clear();
            string m = messageCP.Text;
            byte[] mArr = Encoding.ASCII.GetBytes(m);
            byte[] md5 = MD5.Create().ComputeHash(mArr);
         
            byte[] _s = new byte[signatureCP.Text.Length];
            int i = 0;
            foreach (char c in signatureCP.Text)
            {
                _s[i++] = (byte)c;
            }
            
            byte[] _m = rsa.crypto(_s);

            consoleCP2.AppendText(Encoding.ASCII.GetString(md5) + "\n");
            consoleCP2.AppendText("\n");
            consoleCP2.AppendText(Encoding.ASCII.GetString(_m)+"\n");

            if (_m.Length == md5.Length)
            {
                int j = 0;
                foreach (byte buf in md5)
                {
                    if (buf == _m[j]) j++;
                    
                }
                if(j==md5.Length)
                    consoleCP2.AppendText("\nСообщение достоверно!\n");
                else
                    consoleCP2.AppendText("\nСообщение ложно либо утерена его часть!\n");
            }else
                consoleCP2.AppendText("\nСообщение ложно либо утерена его часть!\n");
           
        }

       
       
    }
}
