﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;

namespace MyRSA
{
    class Eratosfent
    {
        static uint[] table;
        public List<uint> simple;
        static void reset_bit(long bit_number)
        {
            long i = bit_number / 32;
            long p = bit_number % 32;
            table[i] = table[i] & (uint)(~(1 << (int)p));
        }

        static bool get_bit(long bit_number)
        {
            long i = bit_number / 32;
            long p = bit_number % 32;
            if ((table[i] & (uint)(1 << (int)p)) != 0)
                return true;
            return false;
        }

        public Eratosfent(int size)//конструктор
        {
            DateTime dt = DateTime.Now;
            simple = new List<uint>();
            table = new uint[size];
            uint bit_count = (uint)table.Length * (uint)32;

            uint i, j;

            for (i = 0; i < table.Length; i++)
                table[i] = 0xffffffff;

            for (i = 2; i * i <bit_count; i++)
                if (get_bit(i))
                    for (j = i * 2; j < bit_count; j += i)
                        reset_bit(j);

            

            for (i = 2; i < bit_count; i++)
                if (get_bit(i))
                {
                    simple.Add(i);
                } 
        }
    }
}
