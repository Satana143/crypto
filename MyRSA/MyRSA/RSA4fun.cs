﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;

namespace MyRSA
{
    class RSA4fun
    {
        const int Key = 128;

        BigInteger p,q;
        public BigInteger e, d, n;
        public RSA4fun()
        {
           
        }
        
        //расширеный алгоритм Евклида (для нахождения d)
        private BigInteger gcd(BigInteger a, BigInteger b, ref BigInteger x, ref BigInteger y)
        {
	            if (a == 0) {
		            x = 0;
                    y = 1;
		            return b;
	            }

                BigInteger x1 = 0;
                BigInteger y1 = 0;
                BigInteger d = gcd(b % a, a, ref x1, ref y1);

	                x = y1 - (b / a) * x1;
	                y = x1;
	        return d;
        }
        
        //инициализация ключей
        public void initKey()
        {
            Random rnd = new Random();
            MyRC4 r = new MyRC4();
            BigInteger p = GenerateBigPrime(r);
            BigInteger q = GenerateBigPrime(r);
            n = p * q;        
            
            BigInteger phi = (p - 1) * (q - 1);

           do{
            e = rnd.Next(1, 50000);
           }while(!PrimalityTest.IsPrime(e));

            BigInteger x = 0;
            BigInteger y = 1;
            BigInteger buf= gcd(e, phi, ref x, ref y);
            d = (x+(phi*e))%phi;
        }

        private BigInteger GenerateBigPrime(MyRC4 r)
        {
           
            byte[] b = new byte[128 / 2];
            BigInteger n;
            do
            {
                r.NextBytes(b);
                b[0] |= 1;
                b[b.Length - 1] &= 0x7f;
                n = new BigInteger(b);
            }
            while (!PrimalityTest.IsPrime(n));
            return n;
        }

        private BigInteger RandomBigInteger(byte[] b, MyRC4 r)
        {
            r.NextBytes(b);
            b[b.Length - 1] &= 0x7f;
            return new BigInteger(b);
        }

        //шифрование
        public byte[] crypto(byte[] text)
        {
            BigInteger cryptoText = new BigInteger(text);
            BigInteger crypto = BigInteger.ModPow(cryptoText, e, n);
            return crypto.ToByteArray();
        }
        //расшифровка
        public byte[] decrypt(byte[] text)
        {

            BigInteger decryptText = new BigInteger(text);
            BigInteger decrypt = BigInteger.ModPow(decryptText, d, n);
            return decrypt.ToByteArray();
        }

        
        public byte[] code(byte[] text, BigInteger e, BigInteger n)//Кодирование одним методом
        {

            BigInteger codetText = new BigInteger(text);
            BigInteger codeMess = BigInteger.ModPow(codetText, e, n);
            return codeMess.ToByteArray();
        }
    }

}
