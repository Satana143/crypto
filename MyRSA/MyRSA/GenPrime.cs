﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using System.Security.Cryptography;

namespace MyRSA
{
    class GenPrime
    {
       
        public GenPrime()
        {
          
        }
       //2^(2^n)+1
        
        public BigInteger genBigPrimeNumber(int size)
        {
            byte[] N = new byte[size];
            byte[] S = new byte[size];

            Random rnd = new Random();
            rnd.NextBytes(N);
            rnd.NextBytes(S);
            
            BigInteger _N = new BigInteger(N);
            BigInteger _S = new BigInteger(S);
            BigInteger _R =new BigInteger();

            if (_N < 0) _N *= (-1);
            if (_N % 2 == 0) _N++;

            if (_S < 0) _S *= (-1);
            if (_S % 2 == 0) _S++;
            BigInteger aPow;
            do
            {
                _R = rnd.Next((4 * (int)_S) + 2);
                long a = rnd.Next(1, (int)_S);
                aPow = BigInteger.Pow(a, (int)_N - 1);
                 

            } while (_N - 1 != _S * _R);
            if (BigInteger.ModPow(aPow, 1, _N) == 1) 
                return _N;
            else
                genBigPrimeNumber(size);
            return 0;
        }

        private bool RabinMiller(BigInteger key, uint round)
        {
            Random rnd = new Random();
            if (key < 2) return false;

            BigInteger m = key;
            BigInteger t=new BigInteger(0);           
            uint r = round;
            uint s = 0;
            BigInteger x = 0;

            while (m - 1 != powmod(2, (long)s) * t)
            {
                s = (uint)rnd.Next((int)key);
                t = rnd.Next((int)key);
                if (t % 2 == 0) t++;
            }

            for (uint i = r; i > 0; i--)
            {
                BigInteger a = rnd.Next(2, (int)t);
                x = BigInteger.ModPow(a, t, m);
                if (x == 1 || x == m - 1) continue;

                for (uint j = s-1; j > 0; j--)
                {
                    x = BigInteger.ModPow(x, 2, m);
                    if (x == 1) return false;
                    if (x == m - 1) break;
                }
                return false;
            }
           return true;
        }

        bool isPrime(int p)
        {

            for (int a = 2; a < Math.Sqrt(p); a++)
            {
                long buf = powmod(a, p - 1, p);
                if (buf == 1)
                    return true;

            }
            return false;
        }

        bool isPrime(BigInteger p)
        {

            for (BigInteger a = 2; a < p/4; a++)
            {
                BigInteger buf = BigInteger.ModPow(a, p - 1, p);
                if (buf == 1)
                    return true;

            }
            return false;
        }

        long powmod(long a, long k, long n)
        {
            long b = 1;

            while (k != 0)
            {
                if (k % 2 == 0)
                {
                    k /= 2;
                    a = (a * a) % n;
                }
                else
                {
                    k--;
                    b = (b * a) % n;
                }
            }
            return b;
        }

        long powmod(long a, long k)
        {
            long b = 1;

            while (k != 0)
            {
                if (k % 2 == 0)
                {
                    k /= 2;
                    a = (a * a);
                }
                else
                {
                    k--;
                    b = (b * a);
                }
            }
            return b;
        }
    }
}
