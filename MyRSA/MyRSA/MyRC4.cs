﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyRSA
{
    class MyRC4
    {
        byte[] S;
        int k;
        int l;

        const int nVal = byte.MaxValue + 1;

        public MyRC4(byte[] key = null)
        {
            if (key == null)
            {
                DateTime currDate = DateTime.Now;
                long ticks = currDate.Ticks;
                key = BitConverter.GetBytes(ticks);
            }

            int l = key.Length;
            S = new byte[nVal];
            for (int i = 0; i < nVal; i++)
                S[i] = (byte)i;

            int j = 0;
            for (int i = 0; i < nVal; i++)
            {
                j = (j + S[i] + key[i % l]) % nVal;
                Swap(i, j);
            }
        }

        public byte NextByte()
        {
            k = (k + 1) % nVal;
            l = (l + S[k]) % nVal;
            Swap(k, l);
            return S[(S[k] + S[l]) % nVal];
        }

        public void NextBytes(byte[] b)
        {
            for (int i = 0; i < b.Length; i++)
                b[i] = NextByte();
        }

        private void Swap(int i, int j)
        {
            byte t = S[i];
            S[i] = S[j];
            S[j] = t;
        }
    }
}
