#include <iostream>
#include <fstream>
#include <string>

using namespace std;

string getNewFileName(string file){
    //генерим новое имя файла

    int poz=file.find(".",0);
    string prefix = file.substr(poz,file.length()-poz);
    string newfile=file.substr(0,poz);
    newfile+="_copy"+prefix;
    return newfile;
}

int main(int argc, char **argv)
{
    string file,newfile;

    if(argc>1){
        file=argv[1];
        if(argc==3 && argv[2]!=""){
            newfile=argv[2];
        }else{
            newfile=getNewFileName(file);
        }
    }else{
        cout << "Path to file!"<<endl;
        cin >> file;
        newfile=getNewFileName(file);
    }

    ifstream in(file.c_str(), ios::in | ios::binary); //входящий поток


    ofstream out(newfile.c_str(),ios::out|ios::binary); //исходящий поток

    //корректность открытия файла
    if(!in){
        cout<< "Error open file!"<<endl;
        return 1;
    }
    if(!out){
        cout<<"File not create!"<<endl;
        return 2;
    }

    char buf; //буфер

    //копируем файл
    while(in){
        in.get(buf);
        if(in) out.put(buf);
    }

    in.close();
    out.close();
    cout<<"Copy is complete! :)"<<endl;

    return 0;
}
